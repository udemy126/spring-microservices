package com.phatvu.springmicroservices.repository;

import com.phatvu.springmicroservices.pojo.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Integer> {
}
