package com.phatvu.springmicroservices.repository;

import com.phatvu.springmicroservices.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
