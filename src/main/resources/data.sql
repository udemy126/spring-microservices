insert into user_details(id, birth_date, name)
values (10001, current_date(), 'phat');

insert into user_details(id, birth_date, name)
values (10002, current_date(), 'uyen');

insert into user_details(id, birth_date, name)
values (10003, current_date(), 'anh');

insert into post(id, description, user_id)
values (20001, 'I am learning AWS', 10001);
insert into post(id, description, user_id)
values (20002, 'AWS is awesome', 10001);
insert into post(id, description, user_id)
values (20003, 'Love Java programming language', 10002);
insert into post(id, description, user_id)
values (20004, 'DevOps culture', 10003);